import React from 'react';
import ReactJson from 'react-json-view';
import TextareaAutosize from 'react-textarea-autosize';

import axios from 'axios';

import './Home.scss';

type RequestType = 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';

interface HomeState {
    sslChecked: boolean;
    domain?: string;
    route?: string;
    body?: any;
    result?: Object;
    requestType: RequestType;
}

export class Home extends React.Component<{}, HomeState> {
    constructor(props: any) {
        super(props);
        this.state = {
            domain: 'localhost:3000',
            route: '',
            sslChecked: false,
            requestType: 'GET',
        };
        this.context = { error: 'oh no ~!'};
    }

    onChecked(event: any) {
        this.setState({ sslChecked: event.target.checked });
    }

    onChangeTextarea(value: string) {
        this.setState({ body: value });
    }

    keyDown(event: any) {
        if (event.key === 'Tab' && event.keyCode !== 229) {
            event.preventDefault();
            const textareaElement: HTMLTextAreaElement = event.target;

            const start = textareaElement.selectionStart;

            const spaceCount = 2;

            const value = textareaElement.value;
            const before = value.substring(0, start);
            const after = value.substring(start);

            this.setState({ body: before + Array(spaceCount + 1).join(' ') + after }, () => {
                textareaElement.setSelectionRange(start + spaceCount, start + spaceCount);
            });
        }
    }

    createRequestValue(): string {
        return `http${this.state.sslChecked ? 's' : ''}://${this.state.domain}/${this.state.route}`;
    }

    request(): Promise<any> {
        switch(this.state.requestType) {
            case 'GET': {
                return axios.get(this.createRequestValue());
            }
            case 'POST': {
                return axios.post(this.createRequestValue(), { ...JSON.parse(this.state.body) });
            }
            case 'PUT': {
                return axios.put(this.createRequestValue(), { ...JSON.parse(this.state.body) });
            }
            case 'PATCH': {
                return axios.patch(this.createRequestValue(), { ...JSON.parse(this.state.body) });
            }
            case 'DELETE': {
                return axios.delete(this.createRequestValue());
            }
        }
    }

    async submit() {
        try {
            const req = await this.request();
            const res = await req;
            console.log('result: ', res);
            this.setState((state, _) => ({ result: res.data } ));
        } catch(err) {
            console.error(err);
        }
    }

    render() {
        return (
            <div className="home-container">
                <div className="home">
                    <div className="input-area">
                        <div className="input-field">
                            <label className="input-label">Domain</label>
                            <input
                                className="input-form"
                                placeholder="your.domain.com"
                                value={this.state.domain}
                                onChange={(event) => this.setState({domain: event.target.value})}
                            />
                        </div>
                        <div className="input-field">
                            <label className="input-label">Route</label>
                            <input
                                className="input-form"
                                value={this.state.route}
                                onChange={(event) => this.setState({ route: event.target.value })}
                                placeholder="your/route"
                            />
                        </div>
                        <div className="input-field">
                            <label className="input-label">Body</label>
                            <TextareaAutosize
                                className="textarea-form"
                                minRows={5}
                                value={this.state.body}
                                onChange={(event) => this.onChangeTextarea(event.target.value) }
                                onKeyDown={(event) => this.keyDown(event)}
                                placeholder='{&#13;&#10;  "a": "aaaaaa"&#13;&#10;  "b": "bbbbbb"&#13;&#10;  "c": "cccccc"&#13;&#10;}'
                            />
                        </div>
                        <div className="input-field">
                            <label className="input-label">ssl</label>
                            <label className="toggle-button">
                                <input
                                    name="sslToggle"
                                    className="toggle-input"
                                    checked={this.state.sslChecked}
                                    onChange={(event) => this.onChecked(event)}
                                    type="checkbox"
                                />
                                <span className={`toggle-slider ${ this.state.sslChecked ? 'checked' : '' }`}></span>
                            </label>
                        </div>
                        <div className="input-field">
                            <label className="input-label">Request Type</label>
                            <select
                                className="select-form"
                                value={this.state.requestType}
                                onChange={(event) => this.setState({ requestType: event.target.value as RequestType })}
                            >
                                <option>GET</option>
                                <option>POST</option>
                                <option>PUT</option>
                                <option>PATCH</option>
                                <option>DELETE</option>
                            </select>
                        </div>
                        <div className="submit-button-field">
                            <button
                                className="submit-button"
                                onClick={() => this.submit()}
                            >Submit</button>
                        </div>
                    </div>
                    <div className="result-area">
                        <ReactJson src={this.state.result || {}}/>
                    </div>
                </div>
            </div>
        );
    };
}

